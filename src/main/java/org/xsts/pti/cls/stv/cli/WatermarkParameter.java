package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.YesNoParameter;

public class WatermarkParameter extends YesNoParameter {
    public static final String  LONG_NAME = "watermark";
    public static final String  SHORT_NAME = "w";
    public static final String  DESCRIPTION = "Will print a watermark on every page";
    public static final String  NAME = LONG_NAME;


    public WatermarkParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}