package org.xsts.pti.cls.stv.plotters;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.core.config.update.FileSystemUpdater;
import org.xsts.core.data.util.Haversine;
import org.xsts.gtfs.gds.data.collections.GTFSRouteList;
import org.xsts.gtfs.gds.data.timesheet.TOStation;
import org.xsts.gtfs.gds.data.timesheet.TOStationList;
import org.xsts.gtfs.gds.data.types.GTFSRoute;
import org.xsts.gtfs.gds.data.util.GTFSRouteSorter;
import org.xsts.pdf.tools.floatval.PTFFillCircle;
import org.xsts.pdf.tools.floatval.PTFFillRect;
import org.xsts.pdf.tools.floatval.PTFText;
import org.xsts.pti.cls.stv.localtypes.XSTSColor;
import org.xsts.pti.cls.stv.localtypes.XSTSColorList;
import org.xsts.pti.cls.stv.localtypes.XSTSFileNames;
import org.xsts.pti.cls.stv.util.CamelCase;
import org.xsts.pti.cls.stv.util.CentralHQFilter;
import org.xsts.pti.cls.stv.util.NumberDetector;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;


public class STVPlotter extends PlotterPrototype{

    public static final String SIGNATURE = "svtp";
    public static final int DISTANCE_FONT = 60;
    public static final double PROXIMITY_LIMIT = 150.0; // the radius of the circle when searching for connections
    public static final int  CONNEXION_LINE_SIZE = 110;


    GTFSRouteList routes = null;
    Map < String /*routeShortName */, Map< String /*routeID*/, TOStationList>> network;
    Map<String,XSTSColorList> colors = null;


    @Override
    public void plot() {
        super.plot();
    }

    public STVPlotter network(Map<String, Map< String, TOStationList>> network) {
        this.network = network;
        return this;
    }

    public STVPlotter colors(Map<String,XSTSColorList > colors){
        this.colors = colors;
        return this;
    }



    public  STVPlotter routes(GTFSRouteList routes) {
        this.routes = routes;
        return this;
    }

    protected String fileName() {
        EnvironmentConfiguration config = EnvironmentConfiguration.getInstance();
        StringBuilder sb = new StringBuilder();
        sb.append(SIGNATURE)
                .append("-")
                .append(config.getString(FileSystemUpdater.AGENCY))
                .append(".pdf");
        return sb.toString();
    }

    protected void processPages(    PDRectangle rect,
                                    PDDocument doc) throws IOException {

        GTFSRouteSorter sorter = new GTFSRouteSorter(network.keySet());
       if (CentralHQFilter.allowSummaryPage()) {
            createSummaryPage(doc);
        }

        for (String routeShortName: sorter.getSortedList()){
            if (!CentralHQFilter.allowRouteShortName(routeShortName))
                continue;
            Map<String, TOStationList> miniMap = network.get(routeShortName);
            for (String routeID: miniMap.keySet()){
                TOStationList stationList = miniMap.get(routeID);
                Map<String, Set<String>> connections = null;
                if (CentralHQFilter.allowStopConnections()){
                    connections = getConnections(routeShortName, stationList, sorter);
                }


                processStationList(doc, stationList, routeShortName, routeID, connections);
            }
        }
    }


    private void createSummaryPage(PDDocument doc) throws IOException {
        int docWidth = 1000;
        int docHeight = 1000;
        PDRectangle rect = new PDRectangle(docWidth, docHeight);
        PDPageContentStream contentStream = addPageAndOpenStream(rect, doc);
        PTFText textPainter = new PTFText(contentStream);

        // PDType1Font.HELVETICA_BOLD  FONT_DEFAULT   FONT_HANA_MIN_A
        PDFont theFont = getFont(FONT_DEFAULT);
        textPainter.at(500,1000-200).font(theFont).size(100).color(Color.black)
                .text("Summary").position(PTFText.CENTER).draw();

        textPainter.size(50);
        textPainter.position(PTFText.RIGHT).text("No. of routes :").at(500,1000-300).draw();
        StringBuilder sb = new StringBuilder();
        sb.append(routes.keySet().size());
        textPainter.position(PTFText.LEFT).text(sb.toString()).at(500,1000- 300).draw();


        contentStream.close();
    }

    protected Map<String, Set<String>> getConnections(  String routeShortName,
                                                        TOStationList stationList,
                                                        GTFSRouteSorter sorter) {

        Map<String, Set<String>> connections = new HashMap<>();
        for (String aRouteShortName: sorter.getSortedList()){
            if ( routeShortName.compareTo(aRouteShortName) == 0)
                continue;
            Map<String, TOStationList> miniMap = network.get(aRouteShortName);
            for (String routeID: miniMap.keySet()){
                TOStationList aStationList = miniMap.get(routeID);
                for ( TOStation station : stationList.getStations()){
                    // if (station.name().compareTo("St-Georges") != 0)
                    //     continue;
                    if ( !connections.containsKey(station.name())){
                        connections.put(station.name(), new HashSet<>());
                    }
                    if (connections.get(station.name()).contains(aRouteShortName) == true) {
                        continue;
                    }



                    for ( TOStation aStation: aStationList.getStations()) {
                        double dist = Haversine.getDistanceCoordLong(station.latitude(),station.longitude(),aStation.latitude(),aStation.longitude());
                        if (PROXIMITY_LIMIT >= dist) {
                            connections.get(station.name()).add(aRouteShortName);
                        }
                    }
                }
            }
        }

        return connections;
    }

    private void processStationList(    PDDocument doc,
                                        TOStationList stationList,
                                        String routeShortName,
                                        String routeID,
                                        Map<String, Set<String>> connections ) throws IOException {

        processStation( doc,
                        stationList,
                        CentralHQFilter.NO_REF_STATION,
                        routeShortName,
                        routeID,
                        connections);


    }

    private void processStation(    PDDocument doc,
                                    TOStationList offsetListSrc,
                                    TOStation refStation,
                                    String routeShortName,
                                    String routeID,
                                    Map<String, Set<String>> connections) throws IOException {

        TOStationList offsetList = offsetListSrc;
        int nb = offsetList.getStations().size();
        Dimensions dimensions = new Dimensions(nb);



        int docWidth = dimensions.getDocumentWidth();
        int docHeight = dimensions.getDocumentHeight();
        int extraConnSpace = 0;

        if (CentralHQFilter.allowStopConnections()) {
            int maxConn = 0;
            for ( String key : connections.keySet()){
                if (connections.get(key).size() > maxConn){
                    maxConn = connections.get(key).size();
                }
            }
            extraConnSpace = CONNEXION_LINE_SIZE * maxConn;
            docHeight += extraConnSpace;
        }


        PDRectangle rect = new PDRectangle(docWidth, docHeight);

        Color routeColor = selectBestColor(routeShortName);
        if ( routeShortName.compareTo("") == 0)
            routeColor = selectBestColor(routeID);

        PDPageContentStream contentStream = addPageAndOpenStream(rect, doc);

        drawChart(contentStream, routeColor, dimensions, offsetList, refStation,routeShortName, routeID, connections, extraConnSpace);

        contentStream.close();
    }
    Color selectBestColor(String routeShortName) {
        Color selectedColor = Color.black;

        if (routeShortName != null && colors != null){
            Color colorCandidate = null;
            for (String key : colors.keySet()){
                if ( key.compareTo(XSTSFileNames.COLOR_DEFAULT) == 0)
                    continue;
                XSTSColorList colorList = colors.get(key);
                XSTSColor color = colorList.get(routeShortName);
                if (color != null)
                    colorCandidate = new Color( Integer.parseInt(color.red()),
                            Integer.parseInt(color.green()),
                            Integer.parseInt(color.blue()));

                if (colorCandidate != null)
                    selectedColor = colorCandidate;
            }
            if (colorCandidate == null ) {
                XSTSColorList colorList = colors.get(XSTSFileNames.COLOR_DEFAULT);
                XSTSColor color = colorList.get(routeShortName);
                if (color != null)
                    colorCandidate = new Color( Integer.parseInt(color.red()),
                            Integer.parseInt(color.green()),
                            Integer.parseInt(color.blue()));

                if (colorCandidate != null)
                    selectedColor = colorCandidate;
            }
        }


        return selectedColor;
    }

    private void drawChart(PDPageContentStream contentStream, Color routeColor,
                           Dimensions dimensions,TOStationList offsetList, TOStation refStation,
                           String routeShortName,String routeID,
                           Map<String, Set<String>> connections,
                           int extraConnSpace)
            throws IOException{
        contentStream.setNonStrokingColor(routeColor); // 02
        // contentStream.fillRect(200+300, 250-50, 300 * nb-300, 100);
        int barHeight = dimensions.getHorizontalBarHeight();

        contentStream.fillRect(dimensions.getHorizontalBarLeft(), dimensions.getHorizontalBarBottom() + extraConnSpace,
                dimensions.getHorizontalBarWidth(), dimensions.getHorizontalBarHeight());

        double totDist = 0.0;
        TOStation startStation = null;
        TOStation endStation = null;


        boolean useHours = false;
        for ( TOStation station: offsetList.getStations()) {
            if ( Math.abs(station.timeOffset()) > 99) {
                useHours = true;
                break;
            }

        }

        int pos = 0;
        final int HALFSIDE = 75;
        for ( TOStation station: offsetList.getStations()) {
            endStation = station;
            pos++;

            int posX = dimensions.getStopPosX(pos);
            int posY = dimensions.getStopPosY(pos) + extraConnSpace;
            PTFFillRect rect = new PTFFillRect(contentStream);
            rect.borderWidth(0).fillColor(routeColor);
            int rectH = HALFSIDE*4/3;
            int rectY = dimensions.getHorizontalBarBottom();
            if ( ((pos-1)  % 5) == 0 ) {
                //rectH = HALFSIDE*5/3;
                rect.fillColor(Color.black);

            }
            rect.origin(posX-HALFSIDE/2,rectY).dimensions(HALFSIDE,rectH);

            PTFFillCircle circle = new PTFFillCircle(contentStream);
            circle.center(posX,posY).borderWidth(2).borderColor(Color.black).radius(100);

            /*
            if (station.timeOffset() != 0 )
                circle.fillColor(routeColor);
            else
                circle.fillColor(Color.black);
            */
            circle.fillColor(routeColor);
            circle.draw();
            //rect.draw();

            PTFText timeOffsetText = new PTFText(contentStream);

            String plusMinus = "";
            if ( station.timeOffset() > 0)
                plusMinus = "+";
            String timeOffset = plusMinus + station.timeOffset().toString();
            if ( useHours) {
                int hours = station.timeOffset() / 60;
                int minutes = station.timeOffset() % 60;

                if ( hours == 0 && minutes == 0 ){
                    timeOffset = "0";
                } else {
                    timeOffset = plusMinus + hours + "h" + (minutes < 10 ? "0" : "") + minutes;
                }

            }

            // PDType1Font.HELVETICA_BOLD    FONT_HANA_MIN_A     FONT_DEFAULT
            PDFont theFont = getFont(FONT_DEFAULT);
            final int FONT_SIZE_RECOMMENDED = 90;
            final int FONT_SIZE_LARGE = 130;

            final int FONT_SIZE_STOCKHOLM = 200;

            timeOffsetText.at(posX,posY).font(theFont).size(FONT_SIZE_RECOMMENDED).position(PTFText.CENTER);
            if (station.timeOffset() == 0){
                timeOffsetText.color(Color.white);
            } else {
                timeOffsetText.color( ComplementaryColor.getWeightedEuclideanComplement(routeColor));
            }
            timeOffsetText.text(timeOffset);
            if ( useHours)
                timeOffsetText.size(60);

            if (CentralHQFilter.allowTimeOffset()) {
                timeOffsetText.draw();
            }

            //String theStationName = compactor.toSmallCaps(station.name());
            //theStationName = translator.translate(theStationName); // TRANSLATE CHARACTERS
            String theStationName = station.name();

            if (CentralHQFilter.forceCamelCase())
                theStationName = CamelCase.toCamelCase(theStationName);

            // Note: I moved the code to a different module. It works, but it is written by other people.
            // Need to find a way to integrate/refer github source code

            // Japanese to romaji
            if (CentralHQFilter.preferRomaji()) {
                // Use the latinizer module against 'theStationName';
            }

            // Chinese to Pin Yin
            if (CentralHQFilter.preferPinYin()) {
                //theStationName = CentralHQFilter.extractStopNamePrefix(theStationName);
                // Use the latinizer module against 'theStationName';
            }

            if (CentralHQFilter.removeStopNamePrefix()) {
                theStationName = CentralHQFilter.extractStopNamePrefix(theStationName);
            }


            System.out.println(theStationName);
            timeOffsetText.text(theStationName).color(Color.black).size(FONT_SIZE_RECOMMENDED)
                    .at(posX,posY+50+100).angle(60).position(PTFText.LEFT).draw();

            if ( pos > 1  && CentralHQFilter.allowInterStopDistance() ){
                double dist = Haversine.getDistanceCoordLong(startStation.latitude(),startStation.longitude(),endStation.latitude(),endStation.longitude());
                totDist += dist;
                int iDist = (int) dist;
                String textDist = "" + iDist + "m";

                int posXD1 = dimensions.getStopPosX(pos);
                int posXD2 = dimensions.getStopPosX(pos-1);
                int posXD = (posXD1 + posXD2) / 2;
                timeOffsetText.text(textDist).color(Color.black)
                        .at(posXD,posY-50-100).angle(00).position(PTFText.CENTER).size(DISTANCE_FONT);

                if (CentralHQFilter.allowInterStopDistance()) {
                    timeOffsetText.draw();
                }

            }


            if (CentralHQFilter.allowStopConnections()) {

                Set<String>  theKeys = connections.get(station.name());
                ArrayList<String> theKeysAsPrinted = new ArrayList<>();
                if (CentralHQFilter.allowStopConnectionSort()){
                    ArrayList<Integer> numericList = new ArrayList<>();
                    ArrayList<String> stringList = new ArrayList<>();

                    for ( String key:theKeys) {

                            if (NumberDetector.detectNumber(key)) {
                                Integer valueConverted = Integer.parseInt(key);
                                numericList.add(valueConverted);
                            } else {
                                stringList.add(key);
                            }


                    }
                    numericList.sort(new Comparator<Integer>(){

                        @Override
                        public int compare(Integer v1, Integer v2) {
                            if (v1 < v2)
                                return -1;
                            else if (v1 > v2)
                                return 1;
                            else
                                return 0;
                        }
                    });

                    stringList.sort(new Comparator<String>(){

                        @Override
                        public int compare(String v1, String v2) {
                            return v1.compareTo( v2);
                        }
                    });

                    for (Integer aValue: numericList){
                        theKeysAsPrinted.add(aValue.toString());
                    }

                    for (String aValue: stringList){
                        theKeysAsPrinted.add(aValue);
                    }

                } else {
                    for ( String key:theKeys) {
                        theKeysAsPrinted.add(key);
                    }
                }

                int cnt = 0;
                //for ( String key: connections.get(station.name())) {
                for ( String key: theKeysAsPrinted) {
                    if ( key.length() > 4)
                        continue;
                    cnt++;
                    Color connColor = selectBestColor(key);
                    Color connColor2 = ComplementaryColor.getWeightedEuclideanComplement(connColor);


                        int x1 = posX-Dimensions.INTER_STOP_DISTANCE_MANY  /3;     //  /4;
                        int y1 = posY-(cnt+1) *CONNEXION_LINE_SIZE - 50;
                        int w1 = Dimensions.INTER_STOP_DISTANCE_MANY *2/3; ///// /2;
                        int h1 = 95; // 80;
                        PTFFillRect filler = new PTFFillRect(contentStream);
                        filler.origin(x1,y1).dimensions(w1,h1).borderColor(Color.black).borderWidth(0);
                        filler.fillColor(connColor).draw();




                    PTFText connexionText = new PTFText(contentStream);
                    connexionText.at(posX,posY-(cnt+1) *CONNEXION_LINE_SIZE ).font(PDType1Font.HELVETICA_BOLD).size(80).position(PTFText.CENTER);
                    connexionText.text(key).color(connColor2);
                    connexionText.draw();
                }
            }

            startStation = station;
        }

        GTFSRoute route = routes.get(routeID);

        String routeLabel = routeShortName;
        if ( route.description() != null &&route.description().length() > routeLabel.length() )
            routeLabel = route.description();
        if ( route.longName() != null &&route.longName().length() > routeLabel.length() )
            routeLabel = route.longName();

        // Note: I moved the code to a different module. It works, but it is written by other people.
        // Need to find a way to integrate/refer github source code

        // Japanese to romaji
        if (CentralHQFilter.preferRomaji()) {
            // Use the latinizer module against 'routeLabel';
        }
        // Chinese to Pin Yin
        if (CentralHQFilter.preferPinYin()) {
            // Use the latinizer module against 'routeLabel';
        }


        // Some labels are huuuuuuge. Let's take care of that
        if ( routeLabel.length() > 50) {
            if ( route.longName() != null && route.longName().length() < 50) {
                routeLabel = route.longName();
            }
        }

        PTFText rotatedText = new PTFText(contentStream);
        int textHeight = Dimensions.ROUTE_SHORT_NAME_HEIGHT;
        if (routeShortName.length() > 10)
            textHeight = Dimensions.ROUTE_SHORT_NAME_HEIGHT_ALT;
        // Auto detect font size so that all the short name gets into the image
        FontSizeDetector sizeShortName = new FontSizeDetector();
        BufferedImage bimage = new BufferedImage(dimensions.getDocumentWidth(), Dimensions.ROUTE_SHORT_NAME_HEIGHT,
                BufferedImage.TYPE_BYTE_INDEXED);
        Graphics2D g2d = bimage.createGraphics();
        sizeShortName.g2d(g2d).limits(dimensions.getDocumentWidth(),Dimensions.ROUTE_SHORT_NAME_HEIGHT);
        sizeShortName.fontName(FONT_DEFAULT).fontStyle(Font.BOLD).text(routeShortName);
        int bestSize = sizeShortName.autodetectSize();
        g2d.dispose();

        // PDType1Font.HELVETICA_BOLD    FONT_HANA_MIN_B    FONT_DEFAULT
        PDFont theFont = getFont(FONT_DEFAULT);

        rotatedText.at(100,dimensions.getRouteShortNameBottom() + extraConnSpace).angle(0)
                .font(theFont)
                .size(Dimensions.ROUTE_SHORT_NAME_HEIGHT).color(routeColor)
                .size(bestSize);
        rotatedText.text(routeShortName).draw();


        rotatedText.text(routeLabel);
// PDType1Font.HELVETICA_BOLD
        rotatedText.at(100,dimensions.getRouteDescBottom() + extraConnSpace).font(theFont)
                .size(Dimensions.ROUTE_DESC_HEIGHT).color(routeColor).draw();

        int xOffset = 100;
        rotatedText.at(xOffset,100).angle(90).font(theFont)
                .size(50).color(Color.black);
        rotatedText.text("Created with XSTS. Travel and tell your story.").draw();

        xOffset += 50;
        rotatedText.at(xOffset,100);
        rotatedText.text("XSTS is an open initiative unrelated to any transit agency.").draw();

        if ( CentralHQFilter.allowTimeOffset()) {
            xOffset += 50;
            rotatedText.at(xOffset,100);
            rotatedText.text("Your current position is the station whose offset is \"0\".").draw();
        }

        if ( CentralHQFilter.allowStopConnections()) {
            xOffset += 50;
            rotatedText.at(xOffset,100);
            rotatedText.text("Connections (change) within a 100m radius.").draw();
        }

        if (CentralHQFilter.allowInterStopDistance() ) {
            System.out.println("tot Dist = " + totDist);
            int f1 = (int)(totDist/100);
            double d1 = f1/10.0;
            double d2 = d1/1.6;


            rotatedText.at(1000,100).angle(0).size(100).text(Double.toString(d1)+" km" + "   " + Double.toString(d2) + " mi").draw();

        }

        System.out.println("===" + route.description() + "===" + " END ");
    }

}
