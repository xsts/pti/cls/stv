package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.YesNoParameter;

public class AllowTimeOffsetParameter extends YesNoParameter {
    public static final String  LONG_NAME = "allow-time-offset";
    public static final String  SHORT_NAME = "ato";
    public static final String  DESCRIPTION = "Will print the number of minutes between consecutive stops";
    public static final String  NAME = LONG_NAME;


    public AllowTimeOffsetParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}