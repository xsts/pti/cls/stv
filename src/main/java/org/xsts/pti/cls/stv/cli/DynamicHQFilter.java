package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */

import java.util.List;

public class DynamicHQFilter {
    String agency = null;
    boolean allowInterStopDistance = false;
    boolean allowStopConnections = false;
    boolean allowTimeOffset = false;
    List<String> routeShortNameFilter = null;
    boolean allowSummaryPage = false;
    boolean watermark = false;
    boolean forceCamelCase = false;
    boolean preferDescriptionOverName = false;
    boolean removeStopNamePrefix = false;
    List<String> stopNamePrefixes = null;
    boolean preferPinYin = false;
    boolean preferRomaji = false;
    String rootDir;

    // Following attributes are initialized once. They are never to be changed.
    private String homeDir;
    private String separator;

    public DynamicHQFilter() {
        initWithDefaultValues();
    }

    private void initWithDefaultValues() {
        homeDir = System.getProperty("user.home");
        separator = System.getProperty("file.separator");

        StringBuilder xstsPath = new StringBuilder();
        xstsPath.append(homeDir)
                .append(separator)
                .append("projects")
                .append(separator)
                .append("xsts");

        rootDir = xstsPath.toString();
    }

    public String agency() {
        return agency;
    }
    public void agency(String agency) {
        this.agency = agency;
    }

    public boolean allowInterStopDistance() {
        return allowInterStopDistance;
    }

    public void allowInterStopDistance(boolean allowInterStopDistance) {
        this.allowInterStopDistance = allowInterStopDistance;
    }

    public boolean allowStopConnections() {
        return allowStopConnections;
    }

    public void allowStopConnections(boolean allowStopConnections) {
        this.allowStopConnections = allowStopConnections;
    }

    public boolean allowTimeOffset() {
        return allowTimeOffset;
    }

    public void allowTimeOffset(boolean allowTimeOffset) {
        this.allowTimeOffset = allowTimeOffset;
    }

    public List<String> routeShortNameFilter() {
        return routeShortNameFilter;
    }

    public void routeShortNameFilter(List<String> routeShortNameFilter) {
        this.routeShortNameFilter = routeShortNameFilter;
    }


    public boolean allowSummaryPage() { return allowSummaryPage; }
    public void allowSummaryPage(boolean allowSummaryPage) {  this.allowSummaryPage = allowSummaryPage; }

    public boolean watermark() {
        return watermark;
    }
    public void watermark(boolean watermark) {
        this.watermark = watermark;
    }

    public boolean forceCamelCase() {
        return forceCamelCase;
    }
    public void forceCamelCase(boolean forceCamelCase) {
        this.forceCamelCase = forceCamelCase;
    }

    public boolean preferDescriptionOverName() {
        return preferDescriptionOverName;
    }
    public void preferDescriptionOverName(boolean preferDescriptionOverName) {  this.preferDescriptionOverName = preferDescriptionOverName; }

    public boolean removeStopNamePrefix() {
        return removeStopNamePrefix;
    }
    public void removeStopNamePrefix(boolean removeStopNamePrefix) {  this.removeStopNamePrefix = removeStopNamePrefix; }

    public List<String> stopNamePrefixes() {
        return stopNamePrefixes;
    }

    public void stopNamePrefixes(List<String> stopNamePrefixes) {
        this.stopNamePrefixes = stopNamePrefixes;
    }

    public boolean preferPinYin() {
        return preferPinYin;
    }
    public void preferPinYin(boolean preferPinYin) {  this.preferPinYin = preferPinYin; }

    public boolean preferRomaji() {
        return preferRomaji;
    }
    public void preferRomaji(boolean preferRomaji) {  this.preferRomaji = preferRomaji; }

    public String rootDir() {
        return rootDir;
    }
    public void rootDir(String rootDir) {
        this.rootDir = rootDir;
    }
}