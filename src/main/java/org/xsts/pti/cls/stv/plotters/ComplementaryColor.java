package org.xsts.pti.cls.stv.plotters;

import java.awt.*;

public class ComplementaryColor {
    private static int tripleMin(int r,int g, int b) {
        int min = r;
        if ( g < min)
            min = g;
        if (b < min)
            min = b;
        return min;
    }

    private static int tripleMax(int r,int g, int b) {
        int max = r;
        if ( g > max)
            max = g;
        if (b > max)
            max = b;
        return max;
    }

    public static int getEuclideanDifference(Color originalColor, Color testColor) {
        int originalRed = originalColor.getRed();
        int originalGreen = originalColor.getGreen();
        int originalBlue = originalColor.getBlue();

        int testRed = testColor.getRed();
        int testlGreen = testColor.getGreen();
        int testBlue = originalColor.getBlue();

        return (originalRed-testRed)*(originalRed-testRed)
                + (originalGreen-testlGreen)*(originalGreen-testlGreen)
                + (originalBlue-testBlue)*(originalBlue-testBlue);
    }

    public static Color getEuclideanComplement(Color originalColor) {

        int distBlack = getEuclideanDifference(originalColor,Color.black);
        int distWhite = getEuclideanDifference(originalColor,Color.white);

        if ( distBlack > distWhite)
            return Color.black;
        else
            return Color.white;
    }


    // Using info from https://en.wikipedia.org/wiki/Color_difference
    public static int getWeightedEuclideanDifference(Color originalColor, Color testColor){
        int originalRed = originalColor.getRed();
        int originalGreen = originalColor.getGreen();
        int originalBlue = originalColor.getBlue();

        int testRed = testColor.getRed();
        int testlGreen = testColor.getGreen();
        int testBlue = originalColor.getBlue();

        int rmed = originalRed + testRed;
        int deltaRed = originalRed - testRed;
        int deltaGreen = originalGreen - testlGreen;
        int deltaBlue = originalBlue - testBlue;

        int delta = (512 + rmed) * deltaRed * deltaRed
                +1024 * deltaGreen * deltaGreen
                +(512 + 255 - rmed) * deltaBlue * deltaBlue;


        return delta;
    }

    public static Color getWeightedEuclideanComplement(Color originalColor) {


        int distBlack = getWeightedEuclideanDifference(originalColor,Color.black);
        int distWhite = getWeightedEuclideanDifference(originalColor,Color.white);

        if ( distBlack > distWhite)
            return Color.black;
        else
            return Color.white;

    }
}
