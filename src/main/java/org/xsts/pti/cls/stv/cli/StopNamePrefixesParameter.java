package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.StringListFilterParameter;

public class StopNamePrefixesParameter extends StringListFilterParameter {
    public static final String  LONG_NAME = "stop-name-prefixes";
    public static final String  SHORT_NAME = "snp";
    public static final String  DESCRIPTION = "A list of prefixes for the stop names. Used for trimming.";
    public static final String  NAME = LONG_NAME;
    public static final String  VALUE_DESCRIPTION = "tc_,N,L,etc";

    public StopNamePrefixesParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION, VALUE_DESCRIPTION);
    }
}