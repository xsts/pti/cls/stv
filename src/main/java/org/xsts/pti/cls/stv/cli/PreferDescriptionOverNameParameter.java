package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.YesNoParameter;

public class PreferDescriptionOverNameParameter extends YesNoParameter {
    public static final String  LONG_NAME = "prefer-description-over-name";
    public static final String  SHORT_NAME = "pdon";
    public static final String  DESCRIPTION = "Use stop description instead of its name";
    public static final String  NAME = LONG_NAME;


    public PreferDescriptionOverNameParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}