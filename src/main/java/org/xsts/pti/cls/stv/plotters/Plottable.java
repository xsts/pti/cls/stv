package org.xsts.pti.cls.stv.plotters;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public interface Plottable {
    void plot();
}
