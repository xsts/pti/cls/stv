package org.xsts.pti.cls.stv.engines;

import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.core.config.update.FileSystemUpdater;
import org.xsts.core.data.util.TextFileLineCounter;
import org.xsts.gtfs.gdl.loaders.BasicDirLoader;
import org.xsts.gtfs.gdl.loaders.CacheLoader;
import org.xsts.gtfs.gdl.loaders.processors.csv.agency.GTFSFirstAgencyProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.cache.GTFSCacheStopTimeProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.cache.GTFSCacheTripProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.cache.GTFSCacheTripSignatureProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.route.GTFSFirstRouteProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.stop.GTFSStopProcessor;
import org.xsts.gtfs.gdl.loaders.processors.csv.stoptime.GTFSStopTimeTripLenProcessor;
import org.xsts.gtfs.gds.data.collections.GTFSAgencyList;
import org.xsts.gtfs.gds.data.collections.GTFSRouteList;
import org.xsts.gtfs.gds.data.collections.GTFSStopList;
import org.xsts.gtfs.gds.data.collections.StringToIntegerMap;
import org.xsts.gtfs.gds.data.links.GTFSRouteTripLink;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;
import org.xsts.gtfs.gds.data.names.GTFSFileNames;
import org.xsts.gtfs.gds.data.timesheet.TOStationRouteAliases;
import org.xsts.pti.cls.stv.localtypes.XSTSColorBuilder;
import org.xsts.pti.cls.stv.localtypes.XSTSColorList;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class EnginePrototype {
    protected EnvironmentConfiguration fastConfig;
    protected Path fastPath = null;
    protected Path pathToGTFS = null;

    protected GTFSAgencyList agencies = null;
    protected GTFSRouteList routes = null;
    protected GTFSStopList stops = null;
    protected Map<String, XSTSColorList> colorListMap = null;


    protected TOStationRouteAliases stationRouteAliases = null;

    protected Boolean debug = false;


    public EnginePrototype() {
    }

    public GTFSRouteList routes() { return this.routes; }
    public Map<String,XSTSColorList> colorListMap() {return this.colorListMap; }
    protected void build() {
        preloadCache(pathToGTFS);
    }

    protected void preLoad() {
        preLoadAgencies();
        preLoadRoutes();
        loadColors();
        preLoadStops();
        loadStationRouteAliases();

    }

    private void loadColors() {
        colorListMap = new XSTSColorBuilder().loadColors(routes).colorMap();
    }

    private void preLoadAgencies() {
        eventPreLoadAgencies();
        loadAgencies();
        eventPostLoadAgencies();
    }



    protected void eventPostLoadAgencies() {}
    protected void eventPreLoadAgencies() {}
    private void loadAgencies() {
        if (debug) System.out.println("loadAgencies begin");

        Path pathToAgencies = Paths.get(fastConfig.getString(FileSystemUpdater.AGENCY_CATALOG_DIR));
        agencies = (GTFSAgencyList) BasicDirLoader.load(pathToGTFS,
                GTFSFileNames.AGENCY,
                new GTFSAgencyList(),
                new GTFSFirstAgencyProcessor(),
                false,
                null);
        if (debug) System.out.println("Agency count: " + agencies.count());
        if (debug) System.out.println("loadAgencies end");
    }

    private void preLoadRoutes() {
        eventPreLoadRoutes();
        loadRoutes();
        eventPostLoadRoutes();
    }

    private void eventPreLoadRoutes() {
    }

    private void loadRoutes() {
        if (debug) System.out.println("loadRoutes begin");
        Integer lineCount = TextFileLineCounter.countLines(pathToGTFS,
                GTFSFileNames.ROUTES);
        routes = (GTFSRouteList)BasicDirLoader.load(pathToGTFS,
                GTFSFileNames.ROUTES,
                new GTFSRouteList(),
                new GTFSFirstRouteProcessor(),
                false,
                lineCount);
        if (debug) System.out.println("Route count: " + routes.count());

        if (debug) System.out.println("loadRoutes end");

    }

    private void eventPostLoadRoutes() {
    }


    private void preLoadStops() {
        eventPreLoadStops();
        loadStops();
        eventPostLoadStops();
    }

    private void eventPreLoadStops() {
    }

    private void loadStops() {
        if (debug) System.out.println("loadStops begin");
        Integer lineCount = TextFileLineCounter.countLines(pathToGTFS,
                GTFSFileNames.STOPS);
        stops = (GTFSStopList)BasicDirLoader.load(pathToGTFS,
                GTFSFileNames.STOPS,
                new GTFSStopList(),
                new GTFSStopProcessor(),
                null,
                lineCount);
        if (debug) System.out.println("Stop count: " + stops.count());

        if (debug) System.out.println("loadStops end");



    }

    private void eventPostLoadStops() {
    }

    private void loadStationRouteAliases() {
        stationRouteAliases =  new TOStationRouteAliases(stops);
    }

    private void preloadCache(Path path) {
        if (debug) System.out.println("preloadCache begin");
        preloadRoutesToCache(path);
        preloadTripsToCache(path);
        preloadStopTimesToCache(path);
        if (debug) System.out.println("preloadCache end");

    }

    protected void preloadRoutesToCache(Path path) {
        GTFSCache bigCache = GTFSCache.fromGlobal();
        for ( String routeID: routes.keySet()){
            bigCache.add(routeID);
        }
    }

    protected void preloadTripsToCache(Path path) {
        if (debug) System.out.println("load trips begin");
        GTFSCache bigCache = GTFSCache.fromGlobal();
        Integer lineCountTrips = TextFileLineCounter.countLines(pathToGTFS,
                GTFSFileNames.TRIPS);
        CacheLoader.load(pathToGTFS,
                GTFSFileNames.TRIPS,
                bigCache,
                new GTFSCacheTripProcessor(),
                null,
                lineCountTrips);
        if (debug) System.out.println("load trips end");
    }

    private void preloadStopTimesToCache(Path path) {
        GTFSCache bigCache = GTFSCache.fromGlobal();

        Map<String,String> reverseLink = bigCache.getReverseLink();
        StringToIntegerMap frequencies = new StringToIntegerMap();

        for ( String key : reverseLink.keySet()){
            Integer zero = 0;
            frequencies.add(key, zero);
        }

        if (debug) System.out.println("load trip signatures begin");
        Integer lineCountStopTimes = TextFileLineCounter.countLines(pathToGTFS,
                GTFSFileNames.STOP_TIMES);
        CacheLoader.load(pathToGTFS,
                GTFSFileNames.STOP_TIMES,
                bigCache,
                new GTFSCacheTripSignatureProcessor(),
                stops,
                lineCountStopTimes);

        if (debug) System.out.println("load trip signatures end");

        bigCache.generateTripSignatures(stops);

        if (debug) System.out.println("load trip time len begin");

        BasicDirLoader.load(pathToGTFS,
                GTFSFileNames.STOP_TIMES,
                frequencies,
                new GTFSStopTimeTripLenProcessor(),
                null,
                lineCountStopTimes);

        if (debug) System.out.println("load trip time len end");

        Map<String, Integer> routeMaxSize = calcRouteMaxSize(bigCache, frequencies);
        Set<String> longestTripSet = calcLongestTripSet(bigCache, frequencies, routeMaxSize);

        frequencies = null;
        routeMaxSize = null;

        if (debug) System.out.println("load stop time  begin");

        CacheLoader.load(pathToGTFS,
                GTFSFileNames.STOP_TIMES,
                bigCache,
                new GTFSCacheStopTimeProcessor(), // WARNING - different  processor
                longestTripSet,
                lineCountStopTimes);
        if (debug) System.out.println("load stop time  end");
    }

    protected  Set<String> calcLongestTripSet(GTFSCache bigCache, StringToIntegerMap frequencies, Map<String, Integer> routeMaxSize) {
        Set<String> longestTripSet = new HashSet<>();
        for ( String routeID :bigCache.getLink().keySet() ) {
            GTFSRouteTripLink routeLink = bigCache.getLink().get(routeID);
            Integer routeLength = routeMaxSize.get(routeID);
            String selectedTrip = null;

            for ( String tripID : routeLink.getLink().keySet()){
                Integer tripLength = frequencies.get(tripID);
                if ( tripLength == routeLength  && selectedTrip == null){
                    selectedTrip = tripID;
                    break;
                }
            }

            if (selectedTrip != null){
                longestTripSet.add(selectedTrip);
            }
        }
        return longestTripSet;
    }

    protected  Map<String,Integer> calcRouteMaxSize(GTFSCache bigCache, StringToIntegerMap frequencies) {
        Map<String,String> reverseLink = bigCache.getReverseLink();
        Map<String, Integer> routeMaxSize = new HashMap<>();
        for ( String key : bigCache.getLink().keySet()){
            Integer zero = 0;
            routeMaxSize.put(key, zero);
        }

        for ( String key : bigCache.getLink().keySet()){
            Integer zero = 0;
            routeMaxSize.put(key, zero);
        }

        for (String key: reverseLink.keySet()){
            String routeID = reverseLink.get(key);
            Integer tripLength = frequencies.get(key);
            Integer routeLength = routeMaxSize.get(routeID);
            if ( tripLength > routeLength)
                routeMaxSize.put(routeID,tripLength);
        }
        return routeMaxSize;
    }


}
