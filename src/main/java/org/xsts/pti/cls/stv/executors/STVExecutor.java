package org.xsts.pti.cls.stv.executors;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.core.config.update.FileSystemUpdater;
import org.xsts.core.config.update.GraphicsUpdater;
import org.xsts.pti.cls.stv.apps.STVApp;

import org.xsts.pti.cls.stv.cli.STVCLIOptions;
import org.xsts.pti.cls.stv.util.AgencyBuilder;

import org.xsts.pti.cls.stv.util.CentralHQFilter;
import org.xsts.tad.registry.world.directory.*;
import org.xsts.tad.registry.world.naming.BaseNameCompactor;

import java.awt.Color;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class STVExecutor {
    protected List<String> cmdLineArgs;
    protected RootGTFSAgency gtfsAgency;
    protected EnvironmentConfiguration config;
    HierarchicalGTFSAgency gtfs;

    // The following will be later declared as interfaces, but for now let's keep it simple, stupid
    protected STVApp app;


    public STVExecutor(String args[]) {
        cmdLineArgs = new ArrayList<>();
        for (String arg : args){
            cmdLineArgs.add(arg);
        }
        init();
    }

    public void run() {
        app.run();
    }

    public void init() {
        initConfig();
        initAgency();
        warmUp();
    }

    public void warmUp() {
        app = new STVApp();
    }

    public void initConfig() {
        config = EnvironmentConfiguration.getInstance();
        initFileSystem();
        initGraphics();
        initTranslators();
    }

    public void initFileSystem() {
         config.source(new FileSystemUpdater())
                    .put(FileSystemUpdater.ROOT_DIR, STVCLIOptions.instance().dynamicHQFilter().rootDir())
                    .put(FileSystemUpdater.DATA_INPUT_DIR, "${ROOT_DIR}/input")
                    .put(FileSystemUpdater.DATA_OUTPUT_DIR, "${ROOT_DIR}/output/stop-trek-voyager")
                    .put(FileSystemUpdater.ROUTE_COLOR_DIR, "${DATA_INPUT_DIR}/colors")
                    .put(FileSystemUpdater.AGENCY_CATALOG_DIR, "${DATA_INPUT_DIR}/agencies");
    }

    public void initGraphics() {
        config.source(new GraphicsUpdater())
                .put(GraphicsUpdater.BACKGROUND_COLOR, Color.black)
                .put(GraphicsUpdater.FOREGROUND_COLOR, Color.blue);
    }

    public void initTranslators() {

    }

    public void initAgency() {

        Path pathOne = Paths.get(config.getString(FileSystemUpdater.ROOT_DIR), "gtfs");
        String pathUrl = pathOne.toString();

        HierarchicalGTFSDirScanner scanner = new HierarchicalGTFSDirScanner();
        scanner.scan(pathUrl);
        gtfs = scanner.gtfs();
        selectCountry();
        selectAgency();
        printAgencyParentage();
    }

    public void selectCountry() {
    }

    public void selectAgency() {

        configureAgency(config, CentralHQFilter.agency());
    }

    public void selectCompactor() {
        config.put(BaseNameCompactor.COMPACT_TRANSLATOR, new BaseNameCompactor());
    }

    public void configureAgency(EnvironmentConfiguration config, String signature) {

        HierarchicalGTFSAgency candidate = gtfs.findAgency(signature);

        config.put(FileSystemUpdater.AGENCY, AgencyBuilder.name(candidate.signature()))
                .put(FileSystemUpdater.GTFS_INPUT_PATH, "${ROOT_DIR}/" + candidate.pathTo());

        selectCompactor();

    }

    void printAgencyParentage(){
        String agencySignature = config.getString(FileSystemUpdater.AGENCY);
        Addressable addressable = gtfs.findAgency(agencySignature);
        AddressableAgency agency = (AddressableAgency)addressable;
        System.out.println(agency.toString());
        if ( agency instanceof DynamicAgency) {
            DynamicAgency dynamicAgency = (DynamicAgency)agency;
            System.out.println(dynamicAgency.name());
        }
    }

    public static void main(String args[]) {
        //STVExecutor executor = new STVExecutor(args);
        //executor.run();
    }
}