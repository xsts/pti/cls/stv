package org.xsts.pti.cls.stv.engines;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public interface Processable {
    void process();
}
