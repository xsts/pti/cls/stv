package org.xsts.pti.cls.stv.localtypes;

import org.xsts.core.data.collections.KeyValueList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class XSTSUniqueCodeIdentifierList implements KeyValueList {
    private Map<String, XSTSUniqueCodeIdentifier> uids;

    public XSTSUniqueCodeIdentifierList(){
        uids = new HashMap<>();
    }

    @Override
    public void add(String id, Object object) {
        add(id, (XSTSUniqueCodeIdentifier) object);
    }

    public void add(String id, XSTSUniqueCodeIdentifier uid){
        uids.put(id, uid);
    }
    public XSTSUniqueCodeIdentifier get(String id) {
        return uids.get(id);
    }
    public boolean isEmpty() { return uids.size() == 0;}
    public XSTSUniqueCodeIdentifier first() {
        return uids.get(uids.keySet().toArray()[0]);
    }
    public Set<String> keySet() { return uids.keySet(); }
}
