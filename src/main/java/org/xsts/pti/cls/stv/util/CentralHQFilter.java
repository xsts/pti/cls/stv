package org.xsts.pti.cls.stv.util;

import org.xsts.gtfs.gds.data.timesheet.TOStation;
import org.xsts.pti.cls.stv.cli.STVCLIOptions;

import java.util.List;

/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 * This will use later a config file
 */
public class CentralHQFilter {
    public static TOStation NO_REF_STATION = new TOStation("No Reference station", 0);

    public static boolean useSyntheticRouteFields() {
        return true;
    }

    public static boolean allowLineNumber(String lineNumber) {

        return false;
    }

    public static boolean allow(String feature){

        return false;
    }

    public static boolean allowSummaryPage() {
        return  STVCLIOptions.instance().dynamicHQFilter().allowSummaryPage();
    }
    public static boolean hasWatermark() { return STVCLIOptions.instance().dynamicHQFilter().watermark();}


    public static boolean allowRouteShortName(String routeShortName) {
        final List<String> routes = STVCLIOptions.instance().dynamicHQFilter().routeShortNameFilter();

        if (routes == null) {
            return true;
        }

        for (String route : routes) {
            if ( route.compareTo(routeShortName) == 0){
                return true;
            }
        }
        return false;
    }

    // Fine control over suppl. Info
    public static boolean allowTimeOffset() {
        return  STVCLIOptions.instance().dynamicHQFilter().allowTimeOffset();
    }

    public static boolean allowInterStopDistance() {
        return  STVCLIOptions.instance().dynamicHQFilter().allowInterStopDistance();
    }

    public static boolean allowStopConnections() {
        return  STVCLIOptions.instance().dynamicHQFilter().allowStopConnections();
    }

    public static boolean allowStopConnectionSort() {
        return true;
    }
    public static String agency() {
        return  STVCLIOptions.instance().dynamicHQFilter().agency();
    }

    public static boolean forceCamelCase() { return STVCLIOptions.instance().dynamicHQFilter().forceCamelCase(); }
    public static boolean removeStopNamePrefix() { return STVCLIOptions.instance().dynamicHQFilter().removeStopNamePrefix();}
    public static String extractStopNamePrefix(String name) {

        final List<String> prefixes = STVCLIOptions.instance().dynamicHQFilter().stopNamePrefixes();

        for ( String prefix: prefixes ) {
            if ( name.toLowerCase().startsWith(prefix)){
                String cut = name.substring(prefix.length());
                char lowCapital = cut.charAt(0);
                if ( lowCapital >= 'a' && lowCapital <= 'z') {
                    char capital = (char) (lowCapital + (char)'A' - (char)'a');
                    StringBuilder  result = new StringBuilder();
                    result.append(capital);
                    result.append(cut.substring(1));
                    cut = result.toString();
                }
                return cut;
            }
        }
        return name;
    }


    public static boolean preferDescriptionOverName() { return STVCLIOptions.instance().dynamicHQFilter().forceCamelCase();}
    public static boolean preferPinYin() { return STVCLIOptions.instance().dynamicHQFilter().preferPinYin();}
    public static boolean preferRomaji() { return STVCLIOptions.instance().dynamicHQFilter().preferRomaji();}
}
