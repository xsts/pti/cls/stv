package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.Parameter;

public class AgencyParameter extends Parameter {
    public static final String  LONG_NAME = "agency";
    public static final String  SHORT_NAME = "a";
    public static final String  DESCRIPTION = "Set the  name of the agency";
    public static final String  VALUE_DESCRIPTION = "Agency name, e.g. mta, ratp, etc.";
    public static final String  NAME = LONG_NAME;

    public AgencyParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION, VALUE_DESCRIPTION);
    }
}