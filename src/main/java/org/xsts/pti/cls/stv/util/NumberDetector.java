package org.xsts.pti.cls.stv.util;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class NumberDetector {
    public static Boolean detectNumber(String text) {
        for ( int pos = 0; pos < text.length(); ++pos) {
            char c = text.charAt(pos);
            if ( c < '0' || c > '9')
                return false;
        }
        return true;
    }
}