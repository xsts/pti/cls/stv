package org.xsts.pti.cls.stv.plotters;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.core.config.update.FileSystemUpdater;
import org.xsts.pdf.tools.floatval.PTFText;
import org.xsts.pti.cls.stv.util.CentralHQFilter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public abstract class PlotterPrototype implements Plottable{
    Map<String, PDFont> availableFonts = new HashMap<>();
    public static final String FONT_GEORGIA = "georgia";
    public static final String FONT_HELVETICA_NORMAL = "helvetica-normal";
    public static final String FONT_HELVETICA_BOLD = "helvetica-bold";
//    public static final String FONT_ARIAL = "arial";

// Move this to the latinizer module
//    public static final String FONT_GEN_JYUU = "gen-jyuu";
//    public static final String FONT_HANA_MIN_A = "hana-min-a";
//    public static final String FONT_HANA_MIN_B = "hana-min-b";

    public static final String FONT_DEFAULT = FONT_HELVETICA_NORMAL;  // FONT_GEORGIA

    public PlotterPrototype() {

        loadFonts();
    }

    private void loadFonts() {
        PDFont helveticaBoldFont = PDType1Font.HELVETICA_BOLD;
        availableFonts.put(FONT_HELVETICA_BOLD,helveticaBoldFont );
        PDFont georgiaFont = loadFontAlternative("/Georgia.ttf");
        availableFonts.put(FONT_GEORGIA,georgiaFont );
        PDFont helveticaFont = loadFontAlternative("/Helvetica-Normal.ttf");
        availableFonts.put(FONT_HELVETICA_NORMAL,helveticaFont );
/* To do: move the fonts to the latinizer module
        PDFont hgenJyuuFont = loadFontAlternative("/Gen-Jyuu-Gothic-Monospace-Light.ttf");
        availableFonts.put(FONT_GEN_JYUU,hgenJyuuFont );

        PDFont hanaMinA = loadFontAlternative("/HanaMinA.ttf");
        availableFonts.put(FONT_HANA_MIN_A,hanaMinA );

        PDFont hanaMinB = loadFontAlternative("/HanaMinB.ttf");
        availableFonts.put(FONT_HANA_MIN_B,hanaMinB );
*/
    }

    public PDFont getFont(String name) {
        final PDFont font  =  availableFonts.get(name);
        return font;
    }

    private static PDFont loadFontAlternative(String location) {
        PDDocument documentMock = new PDDocument();
        //InputStream systemResourceAsStream = ClassLoader.getSystemResourceAsStream(location);
        InputStream systemResourceAsStream = PlotterPrototype.class.getResourceAsStream(location);
        PDFont font;
        try {
            font = PDType0Font.load(documentMock, systemResourceAsStream, true);
        }
        catch (IOException e) {
            throw new RuntimeException("IO exception");
        }
        return font;
    }

    protected String fileName() {
        return "test.pdf";
    }

    protected void processPages(PDRectangle rect, PDDocument doc) throws IOException {
        System.out.println("No luck here! Move away!");
    }

    public void updateInformation(PDDocument doc) {

    }

    @Override
    public void plot() {
        EnvironmentConfiguration config = EnvironmentConfiguration.getInstance();
        File pdfFile = new File(config.getString(FileSystemUpdater.DATA_OUTPUT_DIR),
                fileName());

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(100,100);
            PDPage page = null;
            PDPageContentStream contentStream = null;

            processPages(rect, doc);
            updateInformation(doc);
            doc.save(pdfFile);
            doc.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    protected PDPageContentStream addPageAndOpenStream( PDRectangle rect, PDDocument doc) throws IOException {
        PDPage page = new PDPage(rect);
        doc.addPage(page);
        PDPageContentStream contentStream = new PDPageContentStream(doc, page);
        if (CentralHQFilter.hasWatermark())
            drawWatermark4(contentStream,"XSTS  example", rect);
        return contentStream;
    }

    private void drawWatermark(PDPageContentStream contentStream, String watermark, PDRectangle rect) throws IOException {

        String line1 = "EXtended Information Standard";
        String line2 = "for Transportation Systems";
        String line3 = ("(EXISTS / XSTS)");
        String line4 = "is an Open Initiative";

        Color color = new Color(200,200,200,20);
        float sz = Math.min(rect.getHeight()/17,rect.getWidth()/17);
        PTFText watermarkPainter = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(watermark).position(PTFText.CENTER).angle(0);

        float xx = rect.getWidth()/2;
        float yy = rect.getHeight()/2;
        yy += 5 * sz / 2;
        watermarkPainter.text(line1).at(xx,yy).draw();
        yy -=3 * sz / 2;
        watermarkPainter.text(line2).at(xx,yy).draw();

        yy -=3 * sz / 2;
        watermarkPainter.text(line3).at(xx,yy).draw();
        yy -=3 * sz / 2;
        watermarkPainter.text(line4).at(xx,yy).draw();
    }


    private void drawWatermark2(PDPageContentStream contentStream, String watermark, PDRectangle rect) throws IOException {
        Color color = new Color(200,200,200,20);
        float sz = Math.min(rect.getHeight()/30,rect.getWidth()/30);
        PTFText watermarkPainter = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(watermark).position(PTFText.LEFT).angle(50);
        for ( int xx = 0; xx < rect.getWidth(); ) {
            for ( int yy = 0; yy < rect.getHeight();) {
                yy += rect.getHeight()/4;
                watermarkPainter.at(xx,yy).draw();
            }
            xx += rect.getWidth()/4;
        }

    }

    private void drawWatermark3(PDPageContentStream contentStream, String watermark, PDRectangle rect) throws IOException {
        int channel = 210;
        Color color = new Color(channel,channel,channel,10);
        String line1 = "EXtended Information Standard" ;
        String line2 = "for Transportation Systems (XSTS)";
        String line3 = "is an Open Initiative";
        String line4 = "unrelated to any transit agency";

        float sz = Math.min(rect.getHeight()/30,rect.getWidth()/30);
        PTFText watermarkPainter1 = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(line1).position(PTFText.LEFT).angle(-30);
        PTFText watermarkPainter2 = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(line2).position(PTFText.LEFT).angle(-30);
        PTFText watermarkPainter3 = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(line3).position(PTFText.LEFT).angle(-30);
        PTFText watermarkPainter4 = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(line4).position(PTFText.LEFT).angle(-30);

        float rc4 = rect.getWidth()/16;

        for ( int xx = 0; xx <= rect.getWidth(); ) {
            for ( int yy = 0; yy <= rect.getHeight();) {
                yy += rect.getHeight()/4;
                watermarkPainter1.at(xx,yy).draw();
                watermarkPainter2.at(xx+rc4,yy).draw();
                watermarkPainter3.at(xx+rc4*2,yy).draw();
                watermarkPainter4.at(xx+rc4*3,yy).draw();

            }
            xx += rect.getWidth()/4;
        }

    }



    private void drawWatermark4(PDPageContentStream contentStream, String watermark, PDRectangle rect) throws IOException {
        int channel = 210;
        Color color = new Color(channel,channel,channel,10);
        String line0 = "XSTS";


        //String line3 = "XSTS is an Open Initiative";
        //String line4 = "unrelated to any transit agency";

        float sz = Math.min(rect.getHeight()/20,rect.getWidth()/20);
        ;
        PTFText watermarkPainter0 = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(line0).position(PTFText.LEFT).angle(-30);
       /* PTFText watermarkPainter3 = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(line3).position(PTFText.LEFT).angle(-30);
        PTFText watermarkPainter4 = new PTFText(contentStream)
                .color(color).size(sz).font(PDType1Font.HELVETICA_BOLD)
                .text(line4).position(PTFText.LEFT).angle(-30);

        */
        float rc4 = rect.getWidth()/20;
        float rx4 = rect.getWidth()/20;
        float ry4 = rect.getWidth()/20;

        for ( int xx = 0; xx <= rect.getWidth(); ) {
            for ( int yy = 0; yy <= rect.getHeight();) {
                yy += rect.getHeight()/4;

                watermarkPainter0.at(xx+rx4,yy).draw();
                watermarkPainter0.at(xx+rx4*2,yy+ry4).draw();
                watermarkPainter0.at(xx+rx4*3,yy+ry4*2).draw();
                watermarkPainter0.at(xx+rx4*4,yy+ry4*3).draw();



                //watermarkPainter3.at(xx+rc4*2,yy).draw();
                //watermarkPainter4.at(xx+rc4*3,yy).draw();

            }
            xx += rect.getWidth()/4;
        }

    }

}
