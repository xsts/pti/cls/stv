package org.xsts.pti.cls.stv.plotters;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 * This class could move to a different module
 */
public class Dimensions {
    public static final int DOCUMENT_TOP_MARGIN = 200;
    public static final int DOCUMENT_BOTTOM_MARGIN = 200;
    public static final int DOCUMENT_LEFT_MARGIN = 400;
    public static final int DOCUMENT_RIGHT_MARGIN = 600;
    public static final int DOCUMENT_VERTICAL_WORK_AREA = 2000;

    // The distance between the first and the last stop is no less than 1200
    public static final int INTER_STOP_DISTANCE_MANY = 300;
    public static final int INTER_STOP_DISTANCE_TRIPLET = 1500;
    public static final int INTER_STOP_DISTANCE_PAIR = 2500;

    public static final int CIRCLE_RADIUS = 100;
    public static final int CIRCLE_RIM = 2;
    public static final int HORIZONTAL_BAR_SEMI_HEIGHT = 30;
    public static final int ROUTE_SHORT_NAME_HEIGHT = 500;
    public static final int ROUTE_SHORT_NAME_HEIGHT_ALT = 400;

    public static final int ROUTE_DESC_HEIGHT = 150;



    private int numberOfStops = 2;
    private int leftPosX = 0;
    private int bottomPosY = 0;

    private int interStopDistance = Dimensions.INTER_STOP_DISTANCE_MANY;
    private int leftMargin = Dimensions.DOCUMENT_LEFT_MARGIN;
    private int bottomMargin = Dimensions.DOCUMENT_BOTTOM_MARGIN;

    private int documentWidth = 0;
    private int documentHeight = 0;


    public Dimensions(int numberOfStops){
        this.numberOfStops = numberOfStops;
        calculate();
    }

    private void calculate() {
        switch(numberOfStops){
            case 2:
                interStopDistance = INTER_STOP_DISTANCE_PAIR;
                break;
            case 3:
                interStopDistance = INTER_STOP_DISTANCE_TRIPLET;
                break;
            case 4:
            default:
                interStopDistance =  INTER_STOP_DISTANCE_MANY;
                break;
        }

        leftPosX = leftMargin + CIRCLE_RADIUS + CIRCLE_RIM;
        bottomPosY = bottomMargin + CIRCLE_RADIUS+CIRCLE_RIM;
        documentWidth = DOCUMENT_LEFT_MARGIN + (CIRCLE_RADIUS + CIRCLE_RIM)*2
                + (numberOfStops -1) * interStopDistance + DOCUMENT_RIGHT_MARGIN;
        documentHeight = DOCUMENT_BOTTOM_MARGIN + (CIRCLE_RADIUS + CIRCLE_RIM)*2 +
                DOCUMENT_TOP_MARGIN + DOCUMENT_VERTICAL_WORK_AREA;

    }

    // order between 1 and N;
    public int getStopPosX(int order) {
        return ( leftPosX + (order -1) *interStopDistance );
    }

    public int getStopPosY(int order) {
        return bottomPosY;
    }

    public int getDocumentWidth() { return documentWidth; }
    public int getDocumentHeight() { return documentHeight; }
    public int getHorizontalBarWidth() {return (numberOfStops -1) *interStopDistance; }
    public int getHorizontalBarHeight() { return 2 * HORIZONTAL_BAR_SEMI_HEIGHT; }
    public int getHorizontalBarLeft() { return leftPosX;}
    public int getHorizontalBarBottom() { return bottomPosY - HORIZONTAL_BAR_SEMI_HEIGHT;}
    public int getRouteShortNameBottom() { return getDocumentHeight() - ROUTE_SHORT_NAME_HEIGHT  - DOCUMENT_TOP_MARGIN; }
    public int getRouteDescBottom() { return getDocumentHeight() - DOCUMENT_TOP_MARGIN; }
}
