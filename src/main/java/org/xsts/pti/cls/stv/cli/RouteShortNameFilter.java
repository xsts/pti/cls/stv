package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.StringListFilterParameter;

public class RouteShortNameFilter extends StringListFilterParameter {
    public static final String  LONG_NAME = "route-short-name-filter";
    public static final String  SHORT_NAME = "rsnf";
    public static final String  DESCRIPTION = "Filters routes by using a whitelist";
    public static final String  NAME = LONG_NAME;
    public static final String  VALUE_DESCRIPTION = "1,4,10,... | all";

    public RouteShortNameFilter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION, VALUE_DESCRIPTION);
    }
}