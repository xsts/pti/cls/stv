package org.xsts.pti.cls.stv.apps;

import org.xsts.pti.cls.stv.engines.Processable;
import org.xsts.pti.cls.stv.engines.STVEngine;
import org.xsts.pti.cls.stv.plotters.Plottable;
import org.xsts.pti.cls.stv.plotters.STVPlotter;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class STVApp {
    Processable engine;
    Plottable plotter;

    public STVApp() {
        init();
    }

    void init() {
        initEngine();
        initPlotter();
    }

    public void initEngine() {
        engine = new STVEngine();
    }

    public void initPlotter() {
        plotter = new STVPlotter();
    }

    public void run() {
        eventPreProcess();
        engine.process();
        eventPostProcess();

        eventPrePlot();
        plotter.plot();
        eventPostPlot();
    }

    public void eventPreProcess() {

    }

    public void eventPostProcess() {

    }

    public void eventPrePlot() {
        STVEngine stvEngine = (STVEngine) engine;
        STVPlotter stvPlotter = (STVPlotter)plotter;

        stvPlotter.network(stvEngine.network());
        stvPlotter.colors(stvEngine.colorListMap());
        stvPlotter.routes(stvEngine.routes());

    }

    public void eventPostPlot() {

    }
}
