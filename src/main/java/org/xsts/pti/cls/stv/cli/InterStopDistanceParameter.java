package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */

import org.xsts.cli.options.YesNoParameter;

public class InterStopDistanceParameter extends YesNoParameter {
    public static final String  LONG_NAME = "inter-stop-distance";
    public static final String  SHORT_NAME = "isd";
    public static final String  DESCRIPTION = "Will print the distance between consecutive stops on the schematics";
    public static final String  NAME = LONG_NAME;


    public InterStopDistanceParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}