package org.xsts.pti.cls.stv.localtypes;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.util.Map;

public class XSTSColor {
    private String shortName;
    private String red;
    private String green;
    private String blue;
    private String hex;

    private XSTSColor(String shortName, java.awt.Color color, String hex){
        this.shortName = shortName;
        Integer r = color.getRed();
        this.red = r.toString();
        Integer g = color.getGreen();
        this.green = g.toString();
        Integer b = color.getBlue();
        this.blue = b.toString();
        this.hex = hex;
    }

    public XSTSColor(CSVRecord record, CSVParser parser) {
        Map<String, Integer> positions = parser.getHeaderMap();

        if (positions.containsKey(XSTSColorFields.SHORT_NAME))
            shortName = record.get(positions.get(XSTSColorFields.SHORT_NAME));

        if (positions.containsKey(XSTSColorFields.RED))
            red = record.get(XSTSColorFields.RED);

        if (positions.containsKey(XSTSColorFields.GREEN))
            green = record.get(XSTSColorFields.GREEN);

        if (positions.containsKey(XSTSColorFields.BLUE))
            blue = record.get(XSTSColorFields.BLUE);

        if (positions.containsKey(XSTSColorFields.HEX))
            hex = record.get(XSTSColorFields.HEX);
    }

    public String shortName() { return shortName;}
    public String red() { return red;}
    public String green() { return green;}
    public String blue() { return blue;}
    public String hex() { return hex;}

    public static XSTSColor synthetize(String routeShortName, String routeHexColor ){
        java.awt.Color color = hexToColor(routeHexColor);
        return new XSTSColor(routeShortName,color,routeHexColor);
    }

    private static java.awt.Color hexToColor(String hexValue){
        int rgbEncoded = hexToInt(hexValue);
        int blue = rgbEncoded % 256;
        rgbEncoded -= blue;
        rgbEncoded /= 256;
        int green = rgbEncoded % 256;
        rgbEncoded -= green;
        rgbEncoded /= 256;
        int red = rgbEncoded;
        return new java.awt.Color(red, green, blue);
    }

    private static int hexToInt(String hexValue){
        String candidate = hexValue.toLowerCase();
        char[] hexSix = candidate.toCharArray();
        int rgbEncoded = 0;
        for ( char c : hexSix){
            rgbEncoded *= 16;
            int digitValue = hexDigitToInt(c);
            rgbEncoded += digitValue;
        }
        return rgbEncoded;
    }

    private static int hexDigitToInt(char digit){
        if (digit >= 'a' && digit <= 'f') {
            return digit -'a' + 10;
        } else if (digit >= 'A' && digit <= 'F') {
            return digit -'A' + 10;
        } else if (digit >= '0' && digit <= '9'){
            return digit -'0';
        } else {
            return 0;
        }
    }
}
