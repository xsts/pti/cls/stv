package org.xsts.pti.cls.stv.about;

import org.xsts.pti.cls.stv.cli.STVCLIOptions;
import org.xsts.pti.cls.stv.executors.STVExecutor;

/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class STVModule {
    public static void main(String[] args) {

        STVCLIOptions.parseCommandLine(args);
        STVCLIOptions stvcliOptions = STVCLIOptions.instance();
        if (stvcliOptions.detectHelp()) {
            stvcliOptions.printUsage();
        } else {
            STVExecutor executor = new STVExecutor(args);
            executor.run();
        }
    }
}
