package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.YesNoParameter;

public class ForceCamelCaseParameter  extends YesNoParameter {
    public static final String  LONG_NAME = "force-camel-case";
    public static final String  SHORT_NAME = "fcc";
    public static final String  DESCRIPTION = "Every stop name will be converted to camel case format";
    public static final String  NAME = LONG_NAME;


    public ForceCamelCaseParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}