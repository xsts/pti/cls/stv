package org.xsts.pti.cls.stv.localtypes;

import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.core.config.update.FileSystemUpdater;
import org.xsts.gtfs.gdl.loaders.BasicDirLoader;
import org.xsts.gtfs.gds.data.collections.GTFSRouteList;
import org.xsts.gtfs.gds.data.types.GTFSRoute;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class XSTSColorBuilder {
    EnvironmentConfiguration fastConfig = null;
    Path pathToColors = null;
    Map<String,XSTSColorList> colorListMap = null;

    public XSTSColorBuilder() {
        fastConfig = EnvironmentConfiguration.getInstance();
        pathToColors = Paths.get(fastConfig.getString(FileSystemUpdater.ROUTE_COLOR_DIR),
                fastConfig.getString(FileSystemUpdater.AGENCY));

    }

    public Map<String,XSTSColorList> colorMap() { return colorListMap; }

    public  XSTSColorBuilder loadColors(GTFSRouteList routes) {
        colorListMap = new HashMap<>();
        loadStandardColors(routes);
        loadDecodedColors();
        return this;
    }

    private void loadStandardColors(GTFSRouteList routes) {
        GTFSRoute firstRoute = routes.getFirstRoute();
        XSTSColorList colorsDefault = new XSTSColorList();

        colorListMap.put(XSTSFileNames.COLOR_DEFAULT,colorsDefault);
        String routeHexColor = firstRoute.color();
        if ( routeHexColor != null){
            for ( String routeID : routes.keySet()){
                GTFSRoute route = routes.get(routeID);
                XSTSColor routeColor = XSTSColor.synthetize(route.shortName(), routes.get(routeID).color());
                if (routeColor.hex().compareTo("FFFFFF") == 0)
                    routeColor = XSTSColor.synthetize(route.shortName(),"000000");
                colorsDefault.add(route.shortName(), routeColor);
            }
        }
    }


    private Set<String> buildColorFileSet() {
        Set<String> colorFileSet = new HashSet<>();
        colorFileSet.add(XSTSFileNames.COLOR_BUS);
        colorFileSet.add(XSTSFileNames.COLOR_NIGHTBUS);
        colorFileSet.add(XSTSFileNames.COLOR_SUBWAY);
        colorFileSet.add(XSTSFileNames.COLOR_TRAIN);
        colorFileSet.add(XSTSFileNames.COLOR_TRAM);
        return colorFileSet;
    }

    private void loadDecodedColors() {
        Set<String> colorFileSet = buildColorFileSet();
        for (String colorFile : colorFileSet){
            XSTSColorList colorsLines = loadColor(colorFile);
            if (colorsLines != null)
                colorListMap.put(colorFile,colorsLines);
        }
    }

    private XSTSColorList loadColor( String fileName) {
        XSTSColorList colors =  (XSTSColorList) BasicDirLoader.load( pathToColors,
                fileName,
                new XSTSColorList(),
                new XSTSColorByShortNameProcessor(),
                false,
                null);
        return colors;
    }
}
