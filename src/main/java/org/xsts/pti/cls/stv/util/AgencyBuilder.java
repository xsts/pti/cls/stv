package org.xsts.pti.cls.stv.util;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class AgencyBuilder {
    public final static String SEPARATOR = "-";
    public final static String RAIL = "rail";
    public final static String SUBWAY = "subway";

    public static String name(String agency){
        return agency;
    }
    public static String rail(String agency){
        return name(agency) + SEPARATOR + RAIL;
    }
    public static String subway(String agency){
        return name(agency) + SEPARATOR + SUBWAY;
    }
    public static String rail(String agency, String category){
        return rail(agency)+ SEPARATOR + category;
    }
    public static String subway(String agency, String category){
        return subway(agency) + SEPARATOR + category;
    }
}
