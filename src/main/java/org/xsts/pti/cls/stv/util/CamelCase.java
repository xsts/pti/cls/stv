package org.xsts.pti.cls.stv.util;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class CamelCase {
    public static String toCamelCase(String text) {
        String [] tokens = text.toLowerCase().split(" ");

        boolean first = true;

        StringBuilder sb = new StringBuilder();

        for ( int i = 0; i < tokens.length; ++i) {
            char c = tokens[i].charAt(0);
            char[] chars = tokens[i].toCharArray();

            if ( chars[0] >= 'a' && chars[0] <= 'z')
                chars[0] += 'A' - 'a';

            String camelCaseElement = new String (chars);
            if (!first)
                sb.append(" ");

            sb.append(camelCaseElement);

            first = false;
        }
        return sb.toString();
    }
}