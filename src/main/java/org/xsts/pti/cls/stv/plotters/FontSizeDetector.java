package org.xsts.pti.cls.stv.plotters;

import java.awt.*;

public class FontSizeDetector {
    Graphics2D g2d;
    int limitWidth;
    int limitHeight;
    int bestSize;
    String fontName;
    int fontStyle;
    String text;

    public FontSizeDetector g2d(Graphics2D g2d){ this.g2d = g2d; return this;}
    public FontSizeDetector limits(int limitWidth, int limitHeight) {
        this.limitWidth = limitWidth;
        this.limitHeight = limitHeight;
        return this;
    }
    public FontSizeDetector fontName(String fontName) { this.fontName = fontName; return this;}
    public FontSizeDetector fontStyle(int fontStyle ) { this.fontStyle = fontStyle; return this;}

    public FontSizeDetector() {
        this.bestSize = 10; // startsize
    }
    public FontSizeDetector text(String text) { this.text = text; return this;}

    boolean checkSize(int testSize) {
        Font font =  new Font(fontName,fontStyle,testSize);
        g2d.setFont(font);  //setting font of surface
        FontMetrics fm = g2d.getFontMetrics();

        int w = fm.stringWidth(text);
        int h =  fm.getHeight() + fm.getAscent();

        font = null;
        fm = null;

        int horizontalFactor = w * 100 / limitWidth;
        int verticalFactor = h * 100 / limitHeight;
        if ( horizontalFactor < 90 && verticalFactor < 90)
            return true;
        return false;
    }

    public int autodetectSize() {
        bestSize = 10;
        // First grow by 10;
        int tenSize = 10;
        while ( checkSize(tenSize) == true){
            tenSize *= 10;
        }

        tenSize  /= 10;

        // then by 2
        int twoSize = tenSize;
        while ( checkSize(twoSize) == true){
            twoSize *= 2;
        }
        twoSize /= 2;

        // then by increments of 10
        int incSize = twoSize;
        while ( checkSize(incSize) == true){
            incSize += 5;
        }

        incSize -= 5;

        return incSize;
    }
}
