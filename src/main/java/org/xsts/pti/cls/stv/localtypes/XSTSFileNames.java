package org.xsts.pti.cls.stv.localtypes;

public class XSTSFileNames {
    public static final String AGENCY = "agency.txt";
    public static final String COLOR_BUS = "color.bus.txt";
    public static final String COLOR_NIGHTBUS = "color.nightbus.txt";
    public static final String COLOR_SUBWAY = "color.subway.txt";
    public static final String COLOR_TRAIN = "color.train.txt";
    public static final String COLOR_TRAM = "color.tram.txt";
    public static final String COLOR_DEFAULT= "color.default.txt";
}
