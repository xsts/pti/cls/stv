package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.Parameter;

public class RootDirParameter  extends Parameter {
    public static final String  LONG_NAME = "root-dir";
    public static final String  SHORT_NAME = "rd";
    public static final String  DESCRIPTION = "Set the  path of the XSTS root dir.";
    public static final String  VALUE_DESCRIPTION = "The root dir path";
    public static final String  NAME = LONG_NAME;

    public RootDirParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION, VALUE_DESCRIPTION);
    }
}