package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.YesNoParameter;

public class RemoveStopNamePrefixParameter extends YesNoParameter {
    public static final String  LONG_NAME = "remove-stop-name-prefix";
    public static final String  SHORT_NAME = "rsnp";
    public static final String  DESCRIPTION = "Trims the prefix from the stop name.";
    public static final String  NAME = LONG_NAME;


    public RemoveStopNamePrefixParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}