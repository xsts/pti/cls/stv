package org.xsts.pti.cls.stv.localtypes;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xsts.core.data.collections.KeyValueList;
import org.xsts.gtfs.gdl.loaders.processors.contracts.SimpleKeyRecordProcessor;

public class XSTSColorByShortNameProcessor extends SimpleKeyRecordProcessor {
    @Override
    public void process(CSVParser parser, KeyValueList keyValueList, Object paramOne, Object paramTwo) {
        //       Boolean firstColor = (Boolean)paramOne;
        for (CSVRecord record : parser) {
            XSTSColor color = new XSTSColor(record, parser);
            //if (shortName.compareTo(color.shortName()) != 0)
            //    continue;
            keyValueList.add(color.shortName(),color);
            //break;
        }
    }
}
