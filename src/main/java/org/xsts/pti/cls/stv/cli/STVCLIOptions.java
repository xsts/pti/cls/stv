package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.CommandLine;
import org.xsts.cli.options.HelpFlag;

import java.util.List;


public class STVCLIOptions {
    static STVCLIOptions stvcliOptions = null;
    CommandLine commandLine = null;
    DynamicHQFilter dynamicHQFilter = null;

    STVCLIOptions(String[] args) {
        addOptions();
        parseOptions(args);

    }

    private void buildDynamicHQFilter() {
        dynamicHQFilter = new DynamicHQFilter();

        if (commandLine.optionDetected(AgencyParameter.NAME)) {
            String value = commandLine.optionValue(AgencyParameter.NAME);
            dynamicHQFilter.agency(value);
        }

        if (commandLine.optionDetected(InterStopDistanceParameter.NAME)) {
            boolean value = commandLine.booleanValue(InterStopDistanceParameter.NAME);
            dynamicHQFilter.allowInterStopDistance(value);
        }

        if (commandLine.optionDetected(AllowStopConnectionsParameter.NAME)) {
            boolean value = commandLine.booleanValue(AllowStopConnectionsParameter.NAME);
            dynamicHQFilter.allowStopConnections(value);
        }

        if (commandLine.optionDetected(AllowTimeOffsetParameter.NAME)) {
            boolean value = commandLine.booleanValue(AllowTimeOffsetParameter.NAME);
            dynamicHQFilter.allowTimeOffset(value);
        }

        if (commandLine.optionDetected(RouteShortNameFilter.NAME)) {
            List<String> value = commandLine.stringListValue(RouteShortNameFilter.NAME);
            dynamicHQFilter.routeShortNameFilter(value);
        }

        if (commandLine.optionDetected(AllowSummaryPageParameter.NAME)) {
            boolean value = commandLine.booleanValue(AllowSummaryPageParameter.NAME);
            dynamicHQFilter.allowSummaryPage(value);
        }

        if (commandLine.optionDetected(WatermarkParameter.NAME)) {
            boolean value = commandLine.booleanValue(WatermarkParameter.NAME);
            dynamicHQFilter.watermark(value);
        }


        if (commandLine.optionDetected(ForceCamelCaseParameter.NAME)) {
            boolean value = commandLine.booleanValue(ForceCamelCaseParameter.NAME);
            dynamicHQFilter.forceCamelCase(value);
        }

        if (commandLine.optionDetected(PreferDescriptionOverNameParameter.NAME)) {
            boolean value = commandLine.booleanValue(PreferDescriptionOverNameParameter.NAME);
            dynamicHQFilter.preferDescriptionOverName(value);
        }

        if (commandLine.optionDetected(RemoveStopNamePrefixParameter.NAME)) {
            boolean value = commandLine.booleanValue(RemoveStopNamePrefixParameter.NAME);
            dynamicHQFilter.removeStopNamePrefix(value);
        }

        if (commandLine.optionDetected(StopNamePrefixesParameter.NAME)) {
            List<String> value = commandLine.stringListValue(StopNamePrefixesParameter.NAME);
            dynamicHQFilter.stopNamePrefixes(value);
        }

        if (commandLine.optionDetected(PreferPinYinParameter.NAME)) {
            boolean value = commandLine.booleanValue(PreferPinYinParameter.NAME);
            dynamicHQFilter.preferPinYin(value);
        }

        if (commandLine.optionDetected(PreferRomajiParameter.NAME)) {
            boolean value = commandLine.booleanValue(PreferRomajiParameter.NAME);
            dynamicHQFilter.preferRomaji(value);
        }

        if (commandLine.optionDetected(RootDirParameter.NAME)) {
            String value = commandLine.optionValue(RootDirParameter.NAME);
            dynamicHQFilter.rootDir(value);
        }
    }

    public DynamicHQFilter dynamicHQFilter() {
        return dynamicHQFilter;
    }

    private void parseOptions(String[] args) {

        if (commandLine.checkArguments(args) == false) {
            System.out.println("There is a bad argument here");
            printUsage();
            System.exit(1);
        } else {
            commandLine.parseArguments(args);
            buildDynamicHQFilter();
        }
    }

    private void addOptions() {
        commandLine = new CommandLine();
        commandLine.add(new HelpFlag());
        commandLine.add(new AgencyParameter());
        commandLine.add(new InterStopDistanceParameter());
        commandLine.add(new AllowStopConnectionsParameter());
        commandLine.add(new AllowTimeOffsetParameter());
        commandLine.add(new RouteShortNameFilter());
        commandLine.add(new AllowSummaryPageParameter());
        commandLine.add(new WatermarkParameter());
        commandLine.add(new ForceCamelCaseParameter());
        commandLine.add(new PreferDescriptionOverNameParameter());
        commandLine.add(new RemoveStopNamePrefixParameter());
        commandLine.add(new StopNamePrefixesParameter());

        commandLine.add(new PreferPinYinParameter());
        commandLine.add(new PreferRomajiParameter());
        commandLine.add(new RootDirParameter());

    }

    public static STVCLIOptions instance() {
        if (stvcliOptions == null ) {
            synchronized (STVCLIOptions.class) {
                if (stvcliOptions == null ) {
                    // nothing to do here
                }
            }
        }
        return stvcliOptions;
    }


    public static void parseCommandLine(String[] args) {
        if (stvcliOptions == null ) {
            synchronized (STVCLIOptions.class) {
                if (stvcliOptions == null ) {
                    stvcliOptions = new STVCLIOptions(args);
                }
            }
        }
    }


    public boolean detectHelp() {
        return commandLine.optionDetected(HelpFlag.NAME);
    }

    public void printUsage() {
        commandLine.printUsage("java -jar infotranspub-cls-stv-x.y.z-SNAPSHOT.jar");
    }
}