package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.YesNoParameter;

public class PreferPinYinParameter extends YesNoParameter {
    public static final String  LONG_NAME = "prefer-pin-yin";
    public static final String  SHORT_NAME = "ppy";
    public static final String  DESCRIPTION = "Force conversion from Chinese characters to PinYin";
    public static final String  NAME = LONG_NAME;


    public PreferPinYinParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}