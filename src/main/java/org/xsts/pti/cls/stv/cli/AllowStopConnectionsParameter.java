package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.YesNoParameter;

public class AllowStopConnectionsParameter extends YesNoParameter {
    public static final String  LONG_NAME = "allow-stop-connections";
    public static final String  SHORT_NAME = "asc";
    public static final String  DESCRIPTION = "Will print the connections for every stop";
    public static final String  NAME = LONG_NAME;


    public AllowStopConnectionsParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}