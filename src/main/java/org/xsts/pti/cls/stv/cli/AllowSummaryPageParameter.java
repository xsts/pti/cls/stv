package org.xsts.pti.cls.stv.cli;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.cli.options.YesNoParameter;

public class AllowSummaryPageParameter extends YesNoParameter {
    public static final String  LONG_NAME = "allow-summary-page";
    public static final String  SHORT_NAME = "asp";
    public static final String  DESCRIPTION = "Will print an extra page ";
    public static final String  NAME = LONG_NAME;


    public AllowSummaryPageParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION);
    }
}