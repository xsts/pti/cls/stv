package org.xsts.pti.cls.stv.engines;
/*
 * Group : XSTS/PTI/CLS
 * Project : Stop Trek Voyager
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.core.config.EnvironmentConfiguration;
import org.xsts.core.config.update.FileSystemUpdater;
import org.xsts.gtfs.gds.data.memcache.GTFSCache;
import org.xsts.gtfs.gds.data.timesheet.TOQuickTimeBuilder;
import org.xsts.gtfs.gds.data.timesheet.TOStationList;
import org.xsts.gtfs.gds.data.timesheet.TOStopDescTool;
import org.xsts.gtfs.gds.data.timesheet.TOStopNameTool;
import org.xsts.pti.cls.stv.util.CentralHQFilter;

import java.nio.file.Paths;
import java.util.Map;

public class STVEngine extends EnginePrototype implements Processable {

    Map<String, Map<String, TOStationList>> network = null;

    public STVEngine() {
        super();
        fastConfig = EnvironmentConfiguration.getInstance();
        new GTFSCache().makeGlobal();

        fastPath = Paths.get(fastConfig.getString(FileSystemUpdater.DATA_INPUT_DIR),
                fastConfig.getString(FileSystemUpdater.AGENCY));
        pathToGTFS = Paths.get(fastConfig.getString(FileSystemUpdater.GTFS_INPUT_PATH),
                fastConfig.getString(FileSystemUpdater.AGENCY));
        debug = true;
    }

    @Override
    public void process() {
        preLoad();
        build();
        if (CentralHQFilter.preferDescriptionOverName()) {
            network = TOQuickTimeBuilder.generate(routes, stops, new TOStopDescTool());
        } else {
            network = TOQuickTimeBuilder.generate(routes, stops, new TOStopNameTool());
        }
    }


    public Map<String,Map<String,TOStationList>> network() { return this.network; }
}
