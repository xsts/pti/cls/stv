# Stop Trek Voyager

This is a spin-off. The original monolithic project had dozens of apps. STV is the first stand-alone app. It will generate PDF files from GTFS / XSTS  data. The open architecture of XSTS allows the data to come from several types of sources.

If you have a GTFS data set, STV can generate PDF files that you can print and post in the vehicles or at the stops/stations. Or, you can just keep the PDF file on your phone or on a tablet. Each page corresponds to a distinct route.

While I managed succesfully to  generate the Latin form of Kanji / Chinese characters, the transcoder being written by someone else, I preferred to keep it out of STV source code. The preferRomaji  and preferPinYin command line arguments are intended for transcoding.

Here below you can find the  list of arguments for STV. Note that there are two forms for the same option: a long name and a short name. The former is preceded by a double dash ( - - ) while the latter is prefixed by a single dash ( - ). If at least one optopn is misspelled, the application will stop immediately. Given th'e number of options, I will progressively add them to this file.

# Show the help

[ < --help | -h > ]  

This is the first thing to do when you don't know the behavior of a program. It shows the list of arguments and halts the execution.

# Set the  name of the agency

[ < --agency | -a >	< Agency name, e.g. mta, ratp, etc. > ]		 

Here, the name is the leaf folder containing the uncompressed GTFS files (the .txt ones). It is recommended that you use a name as close to the real one as possible.

# Print the distance between consecutive stops

[ < --inter-stop-distance | -isd >	< yes | y | no | n > ]		    

The stops.txt file contains the  detailed list of the stops, along with their latitude/longitude coordinates. STV calculates the Haversine (geodesic) distance between two consecutive  stops. In most of the cases, it is quite close to the shape distance.

# Print the connections

[ < --allow-stop-connections | -asc >	< yes | y | no | n > ]

Most of the times, the public transit schematics for a given line  ignore the connection with other transit lines. Wouldn't it be nice to show the connections for every stop on the line ?  If that is tha case, then this option is for you.

There are 10 more options.

